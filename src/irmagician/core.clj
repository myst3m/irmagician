(ns irmagician.core
  (:require [clojure.core.async :as async]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.data.json :as json]
            [taoensso.timbre :refer :all]
            [irmagician.port :as p])
  (:import [java.net Socket ServerSocket SocketException InetAddress InetSocketAddress]))

(def CRLF "\r\n")
(def BANK-LENGTH 64)
(def BANK-COUNT 10)

(def ^:dynamic *ir-port* nil)

(defn set-default-ir-port! [port]
  (alter-var-root #'*ir-port* (constantly port)))

(defmacro with-ir-port [port & body]
  `(binding [*ir-port* ~port]
     (if *ir-port*
       (try
         ~@body
         (finally (ir-close!)))
       (throw (IllegalStateException. "Port not opened.")))))

(defn text<-bytes [byte-data]
  (s/trim (s/join (map char byte-data))))

(defn long<-bytes [byte-data & [base]]
  (try
    (Long/parseLong (text<-bytes byte-data) (or base 16))
    (catch Exception e nil)))

(defn ir-wait
  ([success-cond-pattern fail-cond-pattern return-value-cond-pattern]
   (loop [result (-> *ir-port* p/read-bytes (text<-bytes))]
     (debug "ir-wait:" result)
    (cond
      (re-find (re-pattern success-cond-pattern) result) (do (debug result) true)
      (re-find (re-pattern fail-cond-pattern) result) (do (warn result ) false)
      (re-find (re-pattern return-value-cond-pattern) result) (do (debug result) result)
      :else (recur (str result (-> *ir-port* p/read-bytes (text<-bytes))))))))

(defn ir-clear-result []
  (debug "cleared:" (ir-wait "" "" ".*")))

(defmulti ir-call-post-process (fn [async-port op & _] (keyword (s/upper-case (name op)))))

(defmethod ir-call-post-process :D [async-port op & [args]]
  (when-first [arg args]
    (p/read-bytes async-port 3)))

(defmethod ir-call-post-process :B [async-port op & [args]]  true)

(defmethod ir-call-post-process :W [async-port op & [args]]  true)

(defmethod ir-call-post-process :T [async-port op & [args]]
  (let [result (ir-wait  "-" "-" "OK")]
    (/ (* 1000 (- (* (/ 5 1024) (Long/parseLong (first (s/split result #"\r\n")))) 0.4)) 19.53)))

;;(with-ir-port (ir-open :host "berry" :port 9999) (ir-call :T))
(defmethod ir-call-post-process :default [async-port op & [args]]
  (cond-> (ir-wait  "(OK)|(Done)" "(Err)|(overrun)|(Out)" "([0-9]+)")
    false (ir-clear-result)))


(defn ir-call [op & args]
  (when *ir-port*
    (let [opcode (keyword (s/upper-case (name op)))
          command (s/join "," (cons (name opcode) args))]
      (p/write *ir-port* (str command CRLF))
      (ir-call-post-process *ir-port* op args))))


;;;; User function ;;;;
(defn ir-data-length []
  (Long/parseLong (ir-call :I 1) 16))

(defn ir-postscale []
  (Long/parseLong (ir-call :I 6) 10))

(defn ir-version []
  (ir-call :I 0))

(defn ir-temperature []
  (ir-call :T))

(defn ir-set-record-pointer [data]
  (ir-call :N data))

(defn ir-set-postscale [data]
  (ir-call :K data))

(defn ir-set-bank [bank]
  (ir-call :B bank))

(defn ir-set-data-in-bank [offset data]
  (ir-call :W offset data))

(defn ir-dump-in-bank [offset]
  (ir-call :D offset))

(defn ir-bank-memory-seq []
  (sequence  (comp
              (mapcat (fn [x]
                        (when (= 0 (mod x BANK-LENGTH))
                          (ir-set-bank (quot x BANK-LENGTH)))
                        (ir-dump-in-bank (mod x BANK-LENGTH))))
              (partition-all 3))
             (range 0 (* BANK-LENGTH BANK-COUNT))))

(defn ir-load [dir-or-file-path]
  (debug "load: " dir-or-file-path)  
  (let [f (io/file dir-or-file-path)]
      (if (.isDirectory f)
        (loop [fs (filter #(re-find #".json$" (.getPath %)) (file-seq f))
               m {}]
          (if-not (seq fs)
            m
            (recur (rest fs) (assoc m
                                    (keyword (s/replace (.getName (first fs)) #".json$" ""))
                                    (json/read-str (slurp (first fs)) :key-fn keyword)))))
        (json/read-str (slurp f) :key-fn keyword))))

;;;;;;;;;; High level API ;;;;;;;;;;;
(defn ir-port [] *ir-port*)
(defn ir-open? [] (p/open? *ir-port*))

(defn ^:dynamic ir-open
  "Open Ir port. keys :host, :port and :path are supported."
  ([m]
   (apply ir-open (flatten (map vec m))))
  ([k v & opts]
   (let [opts (seq (concat [k v] opts))]
     (debug "open: " opts)
     (let [{:keys [path] :as hopts} (into {} (vec (map vec (partition-all 2 opts))))
           opts (if path (flatten (vec (assoc hopts :baud-rate 9600))) opts)]
       (apply p/open opts)))))

(defn ^:dynamic ir-close!
  "Close Ir port."
  ([]
   (ir-close! *ir-port*))
  ([async-port]
   (debug "close!: " async-port)
   (p/close! async-port)))

(defn ^:dynamic ir-capture
  "Capture Data."
  ([]
   (ir-call :C)))


(defn ^:dynamic ir-save
  "Save memory data to file if file path is given as JSON format."
  ([]
   (let [length (ir-data-length )
         data (take length (map long<-bytes (ir-bank-memory-seq )))
         ps (ir-postscale )
        json-data (json/write-str
                   {:format :raw
                    :freq 38
                    :data data 
                    :postscale ps
                    })]
     json-data))
  ([fname]
   (io/make-parents fname)
   (spit fname (ir-save))))


(defmulti ^:dynamic ir-restore
  "Restore to memory from give file or clojure map."
  (fn [data-source] (class data-source)) )

(defmethod  ir-restore java.lang.String [file-path]
  (ir-restore (json/read-str (slurp file-path) :key-fn keyword)))

(defmethod ir-restore clojure.lang.PersistentArrayMap [attr-map]
  (let [{:keys [format freq data postscale]} attr-map]
    (ir-set-record-pointer (count data))
    (ir-set-postscale  postscale)

    (dotimes [i (count data)]
      (when (= 0 (mod i BANK-LENGTH))
        (ir-set-bank  (quot i BANK-LENGTH)))
      (ir-set-data-in-bank  (mod i BANK-LENGTH) (nth data i)))
    (debug "restored: " format freq postscale data)
    true))

(defmethod ir-restore :default [arg]
  (throw (IllegalArgumentException. "Given data is invalid. " arg)))

(defn ^:dynamic ir-play
  "Send the Ir code. It is useful with the followin form. 
  (with-ir-port (ir-open :path \"/dev/ttyACM0\" (ir-play)))"
  ([]
   (ir-call :P))
  ([data]
   (debug "play: " data)
   (when-not (empty? data) (ir-restore data))
   (ir-call :P)))

(defn ^:dynamic ir-reset
  "Rest IrMagician."
  ([param]
   (ir-call :R param)))

(defn ^:dynamic ir-target-device []
  :irmagician)

(defn ^:dynamic ir-capture-loop
  "Loop capture and save file"
  ([dir filename-coll]
   (ir-capture-loop dir filename-coll 5000))
  ([dir filename-coll wait-time]
   (io/make-parents (str dir "/" "dummy"))
   (println "Will start capturing." filename-coll)
   (Thread/sleep wait-time)
   (loop [fs filename-coll]
    (let [captured? (do (ir-clear-result) (println "Capturing " (first fs)) (ir-capture))
          next-fs (if captured? (rest fs) fs)]

      (when captured? (ir-save (str dir "/" (first fs) ".json")))
      (if (empty? next-fs)
        (println "Finished")
        (do
          (println "sleep " (quot wait-time 1000) " seconds. Remains are " next-fs)
          (Thread/sleep wait-time)
          (recur next-fs)))))))

(defn -main [& args]
  )
