(ns irmagician.port
  (:import [purejavacomm CommPortIdentifier
                         SerialPort
                         SerialPortEventListener
                         SerialPortEvent
            ]
           [jtermios JTermios]
           [jtermios.linux JTermiosImpl]
           [java.io OutputStream
                    InputStream])
  (:require [clojure.core.async :as async]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.data.json :as json]
            [irmagician.serial :as serial]
            [taoensso.timbre :refer :all])
  
  (:import [java.net Socket ServerSocket SocketException InetAddress InetSocketAddress]))

(declare create-async-port)
(declare set-serial-port-params)

(def ^:dynamic *default-read-timeout* 5)
(defmacro with-timeout [t & body]
  `(binding [*default-read-timeout* ~t]
     ~@body))

(defrecord AsyncPort
    [port in out])

(defprotocol Stream
  (-open [this opts])
  (-close! [this])
  (-write [this data])
  (-read-byte [this]) 
  (-open? [this]))

(extend-protocol Stream
  irmagician.serial.Port
  (-open? [this]
    (if-let [path (:path this)]
      (.. (serial/port-identifier path) isCurrentlyOwned)
      false))

  (-open [this {:keys [xf]}]
    (create-async-port this xf))
  
  (-write [this data]
    (try
      (serial/write this data)
      true
      (catch Exception e false false)))

  
  (-read-byte [this]
    (try
      (.read (:in-stream this))
      (catch java.io.IOException e false nil)))

  (-close! [this]
    (try
      (serial/close! this)
      ;; Already closed.Nothing to do
      (catch purejavacomm.PureJavaIllegalStateException e nil)))
  
  Socket
  (-open? [this]
    (not (or (.isClosed this) (.isInputShutdown this) (.isOutputShutdown this))))

  (-write [this data]
    (try
      (let [os (.getOutputStream this)]
        (.write os data)
        (.flush os))
      true
      (catch SocketException e false false)))

  (-read-byte [this]
    (try
      (.read (.getInputStream this))
      (catch SocketException e false nil)))
  
  (-open [this {:keys [xf]}]
    (create-async-port this xf))

  (-close! [this]
    (when-not (.isInputShutdown this) (.shutdownInput this))
    (when-not (.isOutputShutdown this) (.shutdownOutput this))
    (when-not (.isClosed this) (.close this)))

  AsyncPort
  (-open? [{:keys [port in out]}]
    (if  port (-open? port) false))
  
  (-write [{:keys [out]} data]
    (async/>!! out data))

  
  (-read-byte [{:keys [in]}]
    (async/<!! in))


  (-close! [{:keys [port in out] :as async-port}]
    (-close! port)
    (async/close! in)
    (async/close! out))

  clojure.lang.Keyword
  (-open? [_] false)
  
  nil
  (-open? [_] false))

(defn- create-async-port [port & [xf]]
  (let [in-ch (async/chan 16 (comp (mapcat seq) (or xf identity)))
        out-ch (async/chan )
        async-port  (map->AsyncPort {:port port :in in-ch :out out-ch})
        {:keys [in-stream]} port]

    (async/go-loop []
      (let [buffer (byte-array 64)
            length (try
                     (.read in-stream buffer)
                     (catch Exception e -1))]
        (cond
          (= 0 length) nil
          (> 0 length) (do (info "Stream closed")
                           (async/>!! out-ch length))
          (< 0 length) (do (async/>!! in-ch (take length buffer))
                           (recur)))))
    

    (async/go-loop []
      (when-let [data (and (-open? port) (async/<! out-ch))]
        (if (and (number? data) (< data 0))
          (do (-close! port)
              (async/close! in-ch)
              (async/close! out-ch))
          (do
            (-write port data)
            (recur)))))
    
    async-port))

(defn- create-async-port-legacy [port]
  (let [in-ch (async/chan 10)
        out-ch (async/chan 10)
        async-port  (map->AsyncPort {:port port :in in-ch :out out-ch})]

    (async/go-loop []
      (when-let [data (-read-byte port)]
        (when (< 0 data) (async/>! in-ch data))
        (if (< data 0)
          (async/>! out-ch data)
          (recur))))

    (async/go-loop []
      (when-let [data (and (-open? port) (async/<! out-ch))]
        (if (and (number? data) (< data 0))
          (do (-close! port)
              (async/close! in-ch)
              (async/close! out-ch))
          (do
            (-write port data)
            (recur)))))
    async-port))


;;;;;;;;;;;;; For normal usage ;;;;;;;;;;;;;;

(defn open? [port]
  (try
    (-open? port)
    (catch Exception e false false)))

(defn open [& opts]
  (let [{:keys [host port path baud-rate xf]
         :or {port 9999} :as hopts} (into {} (vec (map vec (partition-all 2 opts))))]
    (cond 
      host (-open (Socket. host port) {:xf xf})
      path (doto (-open (apply serial/open path opts) {:xf xf})
             (set-serial-port-params (select-keys hopts [:baud-rate :data-bits :stop-bits :parity]))))))

(defn close! [port]
  (when port (-close! port)))

(defn read-bytes
  ([{:keys [in]} n]
   (doall
    (take n (repeatedly  #(async/<!! in)))))
  ([{:keys [in]}]
   (doall
    (take-while identity
                (repeatedly  #(first (async/alts!! [in (async/timeout *default-read-timeout*)])))))))

(defn write [port data]
  (-write port (cond
                 (string? data) (.getBytes data)
                 (sequential? data) (byte-array data)
                 :else data)))

(defn get-serial-port-params [async-port]
  (when-let [port (-> async-port :port :raw-port)]
    {:baud-rate (.getBaudRate port)
     :stop-bits (.getStopBits port)
     :parity (.getParity port)
     :data-bits (.getDataBits port)}))

(defn set-serial-port-params [async-port m]
  (when-let [port (-> async-port :port :raw-port)]
    (let [g (get-serial-port-params async-port)
          {:keys [baud-rate data-bits stop-bits parity]} (conj g m)]
      (.setSerialPortParams port baud-rate data-bits stop-bits parity))))
