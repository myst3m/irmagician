# irMagician control library for Clojure

irMagician is the device to control consumer electronics which is controllable with Ir (InfraRed) Remote Control like TV, Air conditioner and so on. It can be purchased from http://www.omiya-giken.com .
This library can get users control irMagician.

## Installation
Add the following to your dependencies in your build.boot or project.clj

[![Clojars Project](https://img.shields.io/clojars/v/theorems/irmagician.svg)](https://clojars.org/theorems/irmagician)

## Examples

```clojure
;; Use irmagician namespace.
(require [irmagician.core :refer :all])

;; Open ttyACM0 device and play.
(with-ir-port (ir-open :path "/dev/ttyACM0")
  (ir-play))

;; Capture code.
(with-ir-port (ir-open :path "/dev/ttyACM0")
  (ir-capture))


;; Connect to  port 9999/tcp on host "berry" and play.
;; Serial over TCP/IP software should run on host "berry".
(with-ir-port (ir-open :host "berry" :port 9999)
  (ir-play))

;; Set default port to /dev/ttyACM0.
(set-default-ir-port! (ir-open :path "/dev/ttyACM0"))

;; After users set default port, users don't need to use with-ir-port macro.
(ir-play)
```

Users can use 'socat' to bridge /dev/ttyACMx and port 9999/tcp.
If you are using Debian/Ubuntu/Raspbian, socat can be used as below.
```sh
# apt install socat
# socat /dev/irmagician,b9600,raw,echo=0 tcp-listen:9999,reuseaddr
```

If users want to execute socat automatically when irMagician is inserted,
put these files as udev rule and service of systemd.
Users can find /dev/irmagician linked to /dev/ttyACMx and socat uses it and listens on 9999/tcp.

```console
## /etc/udev/rules.d/100-irmagician.rules 

ATTRS{product}=="*irMagician*", TAG+="systemd", ENV{SYSTEMD_WANTS}+="irmagician.service", SYMLINK+="irmagician"
```

```console
## /etc/systemd/system/irmagician.service

[Unit]
Description=irMagician Serial over TCP/IP service
Requires=dev-irmagician.device
After=dev-irmagician.device

[Service]
Restart=always
Type=simple
ExecStart=/bin/sh -c "socat /dev/irmagician,b9600,raw,echo=0 tcp-listen:9999,reuseaddr"
```


## Functions/Usage

#### Set default port

After users set default port, users don't need to use with-ir-port macro.
```clojure
(set-default-ir-port! (ir-open :host "berry"))
```

#### Return default port
```clojure
(ir-port)
```

#### Binds a port dynamically

`with-ir-port` binds opened port to thread local  '\*ir-port\*'.
If users don't set default port, this macro has to be used.
ir-close! is called at last.

```clojure
(with-ir-port (ir-open :host "berry")
  (ir-play))
```

#### Open irMagician

`ir-open` returns irMagician port instance. If users use :path for direct access to device, :baud-rate, :stopbits, :databits and :parity options are supported. Baud rate is 9600 as default.

```clojure
(ir-open :path "/dev/ttyACM0")
```

```clojure
(ir-open :host "berry" :port 9999)
```


```clojure
;; Hash map can be given.
(ir-open {:host "berry" :port 9999})
```




#### Close/Disconnect irMagician

```clojure
(ir-close!) ;; close default port.
```
Note: `ir-close!` can have 'port' as argument to close one opened unexpectedly.

```clojure
(let [port (ir-open :host "berry")] ;; 9999 is given as default port. 
  (ir-close! port))
```



#### Load IrDA codes from files
`ir-load` can read JSON file formatted as bellow.

```console
{"format":"raw","freq":38,"data":[76,1,77,51,24,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,7,18,7,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,7,5,6,18,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,18,6,153,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,7,5,6,18,6,18,6,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,6,6,6,18,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,18,6],"postscale":100}
```

Each file name is used as key of each code. 
For example, if file is named as 'power.json', this maps to  {:power {...}}.
`ir-load` just read JSON file. Not restore to memory.

```clojure
;; JSON files are in the directory ex. "/tmp/tvcodes".
;; This returns as the following format {:power {...} :mute {...}}.

(ir-load "/tmp/tvcodes")
```
```clojure
;; A file name also can be given.

(ir-load "/tmp/tvcodes/vol-up.json")
```

```clojure
;; URI format is also capable.

(ir-load "http://localhost/tmp/tvcodes/vol-up.json")
```


#### Capture

```clojure
;; Users should set default port by calling 'set-default-ir-port!'

(ir-capture) 
```

```clojure
(with-ir-port (ir-open :host "berry")
  (ir-capture))
```

#### Loop Capture 

Users can capture continuously by given collection which uses as filename.
For the following example, `ir-capture-loop` loops until the 2nd argument is empty.
Each item in 2nd argument is used as file name with suffix ".json" to save data.
And all files are put into the directory pointed by 1st argument.

```clojure
(ir-capture-loop "/tmp/tv" [0 1 2 3 4 'menu 'cs 'bs 'vol-up 'vol-down])
```

```clojure
;; 5000 is milli time the function waits for user operation. The default value is 2000.

(ir-capture-loop "/tmp/tv" ['change-source 'mute] 5000)
```


#### Play 
If users give a Ir code as a first argument, `ir-play` uses the data.

```clojure
(ir-play)
```

```clojure
(with-ir-port (ir-open :host "berry")
  (ir-play))
```
```clojure
;; Read data formatted as JSON in "/tmp/tvcodes" and set root tag as :tv.

(let [codes (ir-load  "/tmp/tvcode")]
  (ir-play (-> codes :power)))
```

#### Save

`ir-save` saves memory data to the file users give.

```clojure
(ir-save "/tmp/out.json")
```

```clojure
;; If no argument is given, ir-save just show the memory data.
(with-ir-port (ir-open :path "/dev/ttyACM0")
  (ir-save))
```

#### Restore

`ir-restore` stores data to memory. 

```clojure
(ir-restore "/tmp/out.json")
```

```clojure
;; ir-restore also can read data on URI format as well as ir-load
(ir-restore "http://localhost/tmp/out.json")
```


```clojure
;; Hash map can be also given.
(with-ir-open (ir-open :host "berry")
  (ir-restore {:format "raw" :freq 38 ...}))
```
#### Clear result

Clear buffer on serial port on unexpected situation.

```clojure
(ir-clear-result)
```

## Low level functions
Users normally don't need to use these functions.

#### Send raw irMagician commands


```clojure
;; Ex. Get version

(ir-call :I 0) ;; returns "1.0.1"
```

```clojure
;; When default port is not set, use with-ir-port as the same as other functions.

(with-ir-port (ir-open :path "/dev/ttyACM0")
  (ir-call :I 0))
```
## Logging
This library uses Timbre. (https://github.com/ptaoussanis/timbre).
Thanks to this library, it becomes easier to configure the log.  
Default log level is ':debug' as default. Please find the site.

```clojure
(set-level! :info) ;; :trace :debug :info :warn :error :fatal :report
```

## License

Copyright © 2016 Tsutomu Miyashita.

Distributed under the Eclipse Public License either version 1.0 or any later version.
